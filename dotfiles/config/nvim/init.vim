

set nowrap
set signcolumn=yes
set termguicolors
set inccommand=split
filetype plugin indent on
syntax on
set cursorline
set autoindent
set number
set relativenumber
set shiftwidth=4
set softtabstop=4
set tabstop=4
set incsearch
set ruler
set wildmenu
set ignorecase
set smartcase
set showmatch
set hidden


" Window navigation
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
nnoremap <C-Q> <C-W><C-Q>
nnoremap <C-W>t :tabnew<CR>
nnoremap <C-W><C-t> :tabnew<CR>

nnoremap <Tab> gt
nnoremap <S-Tab> gT
